﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using FacilityMover.Models;

namespace FacilityMover
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFacilityMoverService" in both code and config file together.
    [ServiceContract]
    public interface IFacilityMoverService
    {
        [OperationContract]
        Task<IEnumerable<Facility>> GetFacilities(string environmentConnectionString);
        Task<bool> DoesUserExist(string environmentConnectionString, string usernameEntry);
        Task<IEnumerable<FacilityTier>> GetFacilityTiers(Guid facilityId, string environmentConnectionString);
        Task<IEnumerable<Item>> GetFacilityItems(Guid facilityId, string connectionStr);
        Task<MoverStatusMessage>MoveFacility(FacilityMoveData moveData, string toEnvironmentName);
    }
}
