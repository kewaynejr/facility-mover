﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class ItemFacilityTier
    {
        public Guid ID { get; set; }
        public Guid ItemID { get; set; }
        public Guid FacilityTierID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
