﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class OrganizationalTierTypeAttributeLayoutContent
    {
        public Guid ID { get; set; }
        public Guid OrganizationalTierTypeAttributeLayoutID { get; set; }
        public Guid OrganizationalTierTypeAttributeDefinitionID { get; set; }
        public int SortOrder { get; set; }
        public int ReadOnly { get; set; }
        public int Required { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
