﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class SwapID
    {
        public Guid OldID { get; set; }
        public Guid NewID { get; set; }
    }
}
