﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class MoverStatusMessage
    {
        public string StatusMessage { get; set; }
        public string CompleteMessage { get; set; }
        public string SuccessMessage { get; set; }
        public string FailedMessage { get; set; }
    }
}
