﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class OrgTierLevelData
    {
        public OrganizationalTier OrganizationalTier { get; set; }
        public OrganizationalTierType OrganizationalTierType { get; set; }
        public OrganizationalTierTypeAttributeLayout OrganizationalTierTypeAttributeLayout { get; set; }
        public OrganizationalTierTypeAttributeLayoutDetail OrganizationalTierTypeAttributeLayoutDetail { get; set; }
        public IEnumerable<OrganizationalTierTypeAttributeLayoutContent> OrganizationalTierTypeAttributeLayoutContent { get; set; }
        public List<OrganizationalTierTypeAttributeLayoutContentDetail> OrganizationalTierTypeAttributeLayoutContentDetail { get; set; }
        public OrganizationalTierTypeAttributeDefinition OrganizationalTierTypeAttributeDefinition { get; set; }
        public OrganizationalTierTypeAttributeDefinitionDetail OrganizationalTierTypeAttributeDefinitionDetail { get; set; }
        public PickList PickList { get; set; }
        public IEnumerable<PickListItem> PickListItem { get; set; }
        public SwapID SwapID { get; set; }
    }
}
