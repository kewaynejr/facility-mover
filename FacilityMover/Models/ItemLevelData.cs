﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class ItemLevelData
    {
        public Item Item { get; set; }
        public ItemType ItemType { get; set; }
        public ItemTypeAttributeLayout ItemTypeAttributeLayout { get; set; }
        public ItemTypeAttributeLayoutDetail ItemTypeAttributeLayoutDetail { get; set; }
        public IEnumerable<ItemTypeAttributeLayoutContent> ItemTypeAttributeLayoutContent { get; set; }
        public List<ItemTypeAttributeLayoutContentDetail> ItemTypeAttributeLayoutContentDetail { get; set; }
        public ItemTypeAttributeDefinition ItemTypeAttributeDefinition { get; set; }
        public ItemTypeAttributeDefinitionDetail ItemTypeAttributeDefinitionDetail { get; set; }
        public PickList PickList { get; set; }
        public IEnumerable<PickListItem> PickListItem { get; set; }
    }
}
