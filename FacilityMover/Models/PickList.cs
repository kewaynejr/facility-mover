﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class PickList
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Protected { get; set; }
        public int Active { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
        public string ExternalReference { get; set; }
        public Guid? interopClientID { get; set; }
    }
}
