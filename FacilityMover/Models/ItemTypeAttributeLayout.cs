﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class ItemTypeAttributeLayout
    {
        public Guid ID { get; set; }
        public Guid ItemTypeID { get; set; }
        public int NumberOfColumns { get; set; }
        public int DortOrder { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
