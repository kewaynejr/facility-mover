﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class FacilityTierTypeAttributeLayout
    {
        public Guid ID { get; set; }
        public Guid FacilityTierTypeID { get; set; }
        public int NumberOfColumns { get; set; }
        public int SortOrder { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
