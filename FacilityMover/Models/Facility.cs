﻿using System;
using FacilityMover.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public sealed class Facility
    {

        public Guid ID { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string SystemOfMeasure { get; set; }
        public string ModelURI { get; set; }
        public Guid FacilityTypeID { get; set; }
        public Guid? OrganizationalTierID { get; set; }
        public Guid? Code1PickListId { get; set; }
        public Guid? Code2PickListId { get; set; }
        public int Active { get; set; }
        public int Deleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }

    }
}
