﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class ItemTypeAttributeLayoutContentDetail
    {
        public Guid ID { get; set; }
        public Guid ItemTypeAttributeLayoutContentID { get; set; }
        public Guid LanguageID { get; set; }
        public string DefaultValue { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
