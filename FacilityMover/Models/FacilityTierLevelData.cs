﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class FacilityTierLevelData
    {
        public FacilityTier FacilityTier { get; set; }
        public FacilityTierType FacilityTierType { get; set; }
        public FacilityTierTypeAttributeLayout FacilityTierTypeAttributeLayout { get; set; }
        public FacilityTierTypeAttributeLayoutDetail FacilityTierTypeAttributeLayoutDetail { get; set; }
        public IEnumerable<FacilityTierTypeAttributeLayoutContent> FacilityTierTypeAttributeLayoutContent { get; set; }
        public List<FacilityTierTypeAttributeLayoutContentDetail> FacilityTierTypeAttributeLayoutContentDetail { get; set; }
        public FacilityTierTypeAttributeDefinition FacilityTierTypeAttributeDefinition { get; set; }
        public FacilityTierTypeAttributeDefinitionDetail FacilityTierTypeAttributeDefinitionDetail { get; set; }
        public PickList PickList { get; set; }
        public IEnumerable<PickListItem> PickListItem { get; set; }
        public SwapID SwapID { get; set; }
    }
}
