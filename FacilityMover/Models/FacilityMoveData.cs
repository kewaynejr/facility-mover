﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class FacilityMoveData
    {
        public string FromConnectionString { get; set; }
        public Facility FromFacility { get; set; }
        public string ToConnectionString { get; set; }
        public string NewFacilityName { get; set; }
        public string Username { get; set; }
        public IEnumerable<Item> FromItems { get; set; }
        public IEnumerable<FacilityTier> FromTiers { get; set; }
    }
}
