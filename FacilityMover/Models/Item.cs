﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class Item
    {
        public Guid ID { get; set; }
        public Guid? ParentItemID { get; set; }
        public Guid FacilityID { get; set; }
        public string ExternalReference { get; set; }
        public Guid? InteropClientID { get; set; }
        public string  Name { get; set; }
        public string Description { get; set; }
        public Guid? ImageDocumentID { get; set; }
        public string AssestIndetifier { get; set; }
        public int AssetNumber { get; set; }
        public Guid ItemTypeID { get; set; }
        public float? GPSLongitude { get; set; }
        public float? GPSLatitude { get; set; }
        public int New { get; set; }
        public int Active { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
