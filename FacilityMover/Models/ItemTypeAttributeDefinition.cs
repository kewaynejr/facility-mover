﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class ItemTypeAttributeDefinition
    {
        public Guid ID { get; set; }
        public string DataType { get; set; }
        public Guid? PickListID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
