﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class PickListItem
    {
        public Guid ID { get; set; }
        public Guid PickListID { get; set; }
        public float Value { get; set; }
        public int SortOrder { get; set; }
        public Guid? FormDocumentID { get; set; }
        public string HexColor { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedByUsername { get; set; }
        public string ExternalReference { get; set; }
        public int Active { get; set; }
    }
}
