﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class FacilityLevelData
    {
        public Facility Facility { get; set; }
        public FacilityType FacilityType { get; set; }
        public FacilityTypeAttributeLayout FacilityTypeAttributeLayout { get; set; }
        public FacilityTypeAttributeLayoutDetail FacilityTypeAttributeLayoutDetail { get; set; }
        public IEnumerable<FacilityTypeAttributeLayoutContent> FacilityTypeAttributeLayoutContent { get; set; }
        public List<FacilityTypeAttributeLayoutContentDetail> FacilityTypeAttributeLayoutContentDetail { get; set; }
        public FacilityTypeAttributeDefinition FacilityTypeAttributeDefinition { get; set; }
        public FacilityTypeAttributeDefinitionDetail FacilityTypeAttributeDefinitionDetail { get; set; }
        public PickList PickList { get; set; }
        public IEnumerable<PickListItem> PickListItem { get; set; }
    }
}
