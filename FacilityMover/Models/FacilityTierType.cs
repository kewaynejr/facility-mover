﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class FacilityTierType
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Active { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ExternalReference { get; set; }
        public Guid? InteropClientID { get; set; }
        public string ModifiedByUsername { get; set; }
    }
}
