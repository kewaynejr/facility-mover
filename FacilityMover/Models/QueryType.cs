﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.Models
{
    public class QueryType
    {
        public bool Facility { get; set; }
        public bool FacilityType { get; set; }
        public bool FacilityTypeAttributeLayout { get; set; }
        public bool FacilityTypeAttributeLayoutContent { get; set; }
        public bool FacilityTypeAttributeLayoutContentDetail { get; set; }
        public bool FacilityTier { get; set; }
        public bool FacilityTierType { get; set; }
        public bool FacilityTierTypeAttributeLayout { get; set; }
        public bool FacilityTierTypeAttributeLayoutContent { get; set; }
        public bool FacilityTierTypeAttributeLayoutContentDetail { get; set; }
        public bool Item { get; set; }
        public bool ItemType { get; set; }
        public bool ItemTypeAttributeLayout { get; set; }
    }
}
