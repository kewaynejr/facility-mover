﻿using System;
using FacilityMover.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.ViewModels
{
    class SelectTiersViewModel
    {
        private readonly IFacilityMoverService _facilityMoverService = new FacilityMoverService();
        public readonly SelectFromViewModel selectFromVM = new SelectFromViewModel();
        public IEnumerable<FacilityTier> FacilityTiers { get; set; }


        public IEnumerable<FacilityTier> GetFacilityTiers(Guid facilityId, string connectionStr)
        {
            IEnumerable<FacilityTier> facilityTiers = Task.Run(() => _facilityMoverService.GetFacilityTiers(facilityId, connectionStr)).Result;

            return facilityTiers;
        }
        public IEnumerable<Item> GetFacilityItems(Guid facilityId, string connectionStr)
        {
            IEnumerable<Item> facilityItems = Task.Run(() => _facilityMoverService.GetFacilityItems(facilityId, connectionStr)).Result;

            return facilityItems;
        }
        public async Task<MoverStatusMessage> MoveFacility(FacilityMoveData moveData, string toEnvironmentName)
        {
            return await  _facilityMoverService.MoveFacility(moveData, toEnvironmentName);
        }
        public bool isNullOrEmpty(FacilityMoveData data)
        {
            if(string.IsNullOrWhiteSpace(data.FromConnectionString))
            {
                return true;
            }
            if(data.FromFacility.ID == null || data.FromFacility.ID == Guid.Empty)
            {
                return true;
            }
            if (string.IsNullOrWhiteSpace(data.ToConnectionString))
            {
                return true;
            }
            if(string.IsNullOrWhiteSpace(data.NewFacilityName))
            {
                return true;
            }
            if (string.IsNullOrWhiteSpace(data.Username))
            {
                return true;
            }

            return false;
        }
    }
}
