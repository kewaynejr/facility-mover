﻿using FacilityMover.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityMover.ViewModels
{
    public class SelectFromViewModel
    {
        private readonly IFacilityMoverService _facilityMoverService = new FacilityMoverService();
        public string ConnectionString { get; set; }
        public Guid FacilityId { get; set; }
        public async Task<IEnumerable<Facility>> GetFacilities(string env)
        {
            IEnumerable<Facility> facilities = new List<Facility>();
            string connectionString = GetConnectionString(env);
            facilities = await _facilityMoverService.GetFacilities(connectionString);

            return facilities;
        }
        public async Task<bool> DoesUserExist(string connectionString, string usernameEntry)
        {
            return await _facilityMoverService.DoesUserExist(connectionString, usernameEntry);
        }
        public string GetConnectionString(string env)
        {
            string connectionStr = "";
            foreach (ConnectionStringSettings config in ConfigurationManager.ConnectionStrings)
            {
                if (env.ToLower() == config.Name)
                {
                    connectionStr = config.ConnectionString;
                }
            }
            ConnectionString = connectionStr;
            return ConnectionString;
        }
    }
}
