﻿using System;
using System.Collections.Generic;
using System.Configuration;
using FacilityMover.ViewModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FacilityMover.Views
{
    /// <summary>
    /// Interaction logic for selectToEnv.xaml
    /// </summary>
    public partial class selectToEnv : UserControl
    {
        public selectToEnv()
        {
            InitializeComponent();
            List<string> environments = new List<string>();
            foreach (ConnectionStringSettings config in ConfigurationManager.ConnectionStrings)
            {
                environments.Add(config.Name.ToUpper());
            }

            environmentList.ItemsSource = environments;
            environmentList.SelectionChanged += EnvironmentList_SelectionChanged2;
        }

        private void EnvironmentList_SelectionChanged2(object sender, SelectionChangedEventArgs e)
        {
            SelectToViewModel selectToVM = new SelectToViewModel();

            throw new NotImplementedException();
        }
    }
}
