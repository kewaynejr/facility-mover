﻿using FacilityMover.Models;
using FacilityMover.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FacilityMover.Views
{
    /// <summary>
    /// Interaction logic for selectFromEnv.xaml
    /// </summary>

    public partial class selectFromEnv : UserControl
    {
        SelectFromViewModel selectFromVM = new SelectFromViewModel();
        SelectTiersViewModel selectedTiersVM = new SelectTiersViewModel();
        FacilityMoveData moveData = new FacilityMoveData();
        public string toEnvironmentName { get; set; }
        public selectFromEnv()
        {
            InitializeComponent();
            loader.Visibility = Visibility.Hidden;
            List<string> environments = new List<string>();
            foreach(ConnectionStringSettings config in ConfigurationManager.ConnectionStrings)
            {
                environments.Add(config.Name.ToUpper());
            }
            environmentList.ItemsSource = environments;
            environmentToList.ItemsSource = environments;
            environmentList.SelectionChanged += EnvironmentList_SelectionChanged1;
            environmentToList.SelectionChanged += EnvironmentToList_SelectionChanged;
        }

        private void EnvironmentList_SelectionChanged1(object sender, SelectionChangedEventArgs e)
        {
            string environment = environmentList.SelectedItem.ToString();
            IEnumerable<Facility> facilities = Task.Run(() => selectFromVM.GetFacilities(environment)).Result;

            facilitytList.ItemsSource = facilities;
            facilityTierList.ItemsSource = null;
            moveData.FromConnectionString = selectFromVM.GetConnectionString(environment);
            facilitytList.SelectionChanged += FacilitytList_SelectionChanged;
        }

        private void EnvironmentToList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string environment = environmentToList.SelectedItem.ToString();
            toEnvironmentName = environment;
            usernamePrompt.Content = "What is your current username in " + toEnvironmentName;

            moveData.ToConnectionString = selectFromVM.GetConnectionString(environment);
        }

        private void FacilitytList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Facility facility = (Facility)e.AddedItems[0];
                moveData.FromFacility = facility;

                IEnumerable<FacilityTier> allFacilityTiers = selectedTiersVM.GetFacilityTiers(facility.ID, moveData.FromConnectionString);

                IEnumerable<Item> allItems = selectedTiersVM.GetFacilityItems(facility.ID, moveData.FromConnectionString);

                moveData.FromFacility.ID = facility.ID;
                facilityItemList.ItemsSource = allItems;
                facilityTierList.ItemsSource = allFacilityTiers;
                moveData.FromTiers = allFacilityTiers;
                moveData.FromItems = allItems;
            }
        }

        public async void startMove_Click(object sender, RoutedEventArgs e)
        {
            moveData.NewFacilityName = newFacilityName.Text.ToString();
            moveData.Username = userName.Text.ToString();

            if (!selectedTiersVM.isNullOrEmpty(moveData))
            {
                bool validUser = await selectFromVM.DoesUserExist(moveData.ToConnectionString, moveData.Username);
                if (validUser)
                {
                    startMove.IsEnabled = false;
                    loader.Visibility = Visibility.Visible;
                    MoverStatusMessage message = await selectedTiersVM.MoveFacility(moveData, toEnvironmentName);

                    responseText.TextAlignment = TextAlignment.Center;
                    if (message.FailedMessage != null)
                    {
                        responseText.Text = message.FailedMessage;
                        responseText.Foreground = Brushes.Red;
                    }
                    else
                    {
                        responseText.Text = message.SuccessMessage;
                        responseText.Foreground = Brushes.Green;
                    }
                    loader.Visibility = Visibility.Hidden;
                    startMove.IsEnabled = true;
                }
                else
                {
                    responseText.Text = "Username '" + moveData.Username + "' doesn't Exsist in " + toEnvironmentName;
                    responseText.Foreground = Brushes.Red;
                }
            }
            else
            {
                responseText.Text = "Please ensure that all fields are filled out";
                responseText.Foreground = Brushes.Red;
            }

        }
    }
}
