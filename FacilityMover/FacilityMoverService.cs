﻿using System;
using System.Collections.Generic;
using FacilityMover.Models;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;

namespace FacilityMover
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FacilityMoverService" in both code and config file together.
    public class FacilityMoverService : IFacilityMoverService
    {
        List<SwapID> orgTierIds = new List<SwapID>();
        List<SwapID> swapTierIds = new List<SwapID>();
        List<SwapID> swapItemIds = new List<SwapID>();

        #region Methods to get data for User
        //Get All Facilities
        public async Task<IEnumerable<Facility>> GetFacilities(string environmentConnectionString)
        {
            List<Facility> facilityList = new List<Facility>();

            try
            {
                using (SqlConnection connection = new SqlConnection(environmentConnectionString))
                {
                    connection.Open();
                    string query = "SELECT * FROM dbo.Facility";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Facility facility = new Facility();
                                facility.Name = reader["Name"].ToString();
                                facility.Description = reader["Description"].ToString();
                                facility.ID = Guid.Parse(reader["ID"].ToString());
                                facility.OrganizationalTierID = Guid.Parse(reader["OrganizationalTierID"].ToString());
                                facility.FacilityTypeID = Guid.Parse(reader["FacilityTypeID"].ToString());
                                facility.SystemOfMeasure = reader["SystemOfMeasure"].ToString();

                                facilityList.Add(facility);
                            }
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }

            return facilityList;
        }


        //Get Facility Tiers
        public async Task<IEnumerable<FacilityTier>> GetFacilityTiers(Guid facilityId, string environmentConnectionString)
        {
            IEnumerable<FacilityTier> facilityTierList = new List<FacilityTier>();

            try
            {
                using(SqlConnection connection = new SqlConnection(environmentConnectionString))
                {
                    connection.Open();
                    string query = "SELECT * FROM dbo.FacilityTier WHERE FacilityID = @facilityId";

                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@facilityId", facilityId, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    IEnumerable<FacilityTier> facilityTier = await connection.QueryAsync<FacilityTier>(query, parameters);
                    facilityTierList = facilityTier;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }

            return facilityTierList;
        }

        public async Task<IEnumerable<Item>> GetFacilityItems(Guid facilityId, string connectionStr)
        {
            IEnumerable<Item> facilityItems = new List<Item>();
            try
            {
                using(SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();
                    string query = "SELECT * FROM dbo.Item WHERE FacilityID = @facilityId";
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@facilityId", facilityId, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    IEnumerable<Item> items = await connection.QueryAsync<Item>(query, parameters);

                    facilityItems = items;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }

            return facilityItems;
        }
        public async Task<bool> DoesUserExist(string connectionStr, string userInput)
        {
            string username = null;
            using (SqlConnection connection = new SqlConnection(connectionStr))
            {
                connection.Open();
                string query = "SELECT * FROM [dbo].[User] WHERE Username = @Username";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.Add("@Username", userInput);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            username = reader["Username"].ToString();
                        }
                    }
                    connection.Close();
                }
            }
            return (username != null);
        }

        #endregion

        #region The move Facility Method
        public async Task<MoverStatusMessage> MoveFacility(FacilityMoveData data, string toEnvironmentName)
        {
            //Old facility name for success message...
            string oldFacilityName = data.FromFacility.Name;
            MoverStatusMessage message = new MoverStatusMessage();
            QueryType queryType = new QueryType();
            try
            {

                //Get Org Tier Data
                IEnumerable<OrgTierLevelData> orgTierData = await GetAllOrgTierLevelData(data);

                //Get Facility Data
                FacilityLevelData facilityLevelData = await GetAllFacilityLevelData(data);
                facilityLevelData.Facility.ID = Guid.NewGuid();

                //Match org tier Id
                SwapID orgIds = orgTierIds.Where(s => s.OldID == facilityLevelData.Facility.OrganizationalTierID).Single();
                facilityLevelData.Facility.OrganizationalTierID = orgIds.NewID;

                //Match org tier parent Id
                foreach(OrgTierLevelData orgData in orgTierData)
                {
                    if (orgData.OrganizationalTier.ParentOrganizationalTierID != null && orgData.OrganizationalTier.ParentOrganizationalTierID != Guid.Empty)
                    {
                        SwapID Ids = orgTierIds.Where(o => o.OldID == orgData.OrganizationalTier.ParentOrganizationalTierID).Single();
                        orgData.OrganizationalTier.ParentOrganizationalTierID = Ids.NewID;
                    }
                }

                //Get Tier Data
                IEnumerable<FacilityTierLevelData> facilityTierLevelData = await GetAllFacilityTierLevelData(data.FromTiers, data.FromConnectionString);

                //Match Tier Ids...
                foreach (FacilityTierLevelData tierdata in facilityTierLevelData)
                {
                    tierdata.FacilityTier.FacilityID = facilityLevelData.Facility.ID;
                    if(tierdata.FacilityTier != null && tierdata.FacilityTier.ParentFacilityTierID != Guid.Empty)
                    {
                        SwapID IDs = swapTierIds.Where(s => s.OldID == tierdata.FacilityTier.ParentFacilityTierID).Single();
                        tierdata.FacilityTier.ParentFacilityTierID = IDs.NewID;
                    }
                }

                //Get Item Data
                IEnumerable<ItemLevelData> itemLevelData = await GetItemLevelData(data.FromItems, data.FromConnectionString);

                //Match Item Ids...
                foreach (ItemLevelData itemData in itemLevelData)
                {
                    itemData.Item.FacilityID = facilityLevelData.Facility.ID;
                    if(itemData.Item.ParentItemID != Guid.Empty && itemData.Item.ParentItemID != null)
                    {
                        SwapID IDs = swapItemIds.Where(s => s.OldID == itemData.Item.ParentItemID).Single();
                        itemData.Item.ParentItemID = IDs.NewID;
                    }
                }

                //Insert Org Tier data
                message = await ExecuteOrgTierMoveQueryAsync(orgTierData, data.ToConnectionString);

                if(message.FailedMessage == null && message.CompleteMessage == "Completed Org Tier Level")
                {
                    //Insert Facility Level data
                    message = await ExecuteFacilityMoveQueryAsync(facilityLevelData, data.ToConnectionString);
                }
                if(message.FailedMessage == null && message.CompleteMessage == "Completed Facility Level")
                {
                    //Insert Tier Level data
                    message = await ExecuteFacilityTierMoveQueryAsync(facilityTierLevelData, data.ToConnectionString);
                }
                if(message.FailedMessage == null && message.CompleteMessage == "Completed Tier Level")
                {
                    //Insert Item Level data
                    message = await ExecuteItemMoveQueryAsync(itemLevelData, data.ToConnectionString);
                }

            }
            catch (Exception e)
            {
                message.FailedMessage = oldFacilityName + " has failed to move to " + toEnvironmentName + " as '" + data.NewFacilityName + "'. Error: " + e.Message;
                return message;
            }

            message.SuccessMessage = oldFacilityName + " has been successfully moved to " + toEnvironmentName + " as '" + data.NewFacilityName + "'";
            message.CompleteMessage = "Complete";

            return message;
        }

        #endregion

        #region Org Tier Level Get Method

        public async Task<IEnumerable<OrgTierLevelData>> GetAllOrgTierLevelData(FacilityMoveData data)
        {
            List<OrgTierLevelData> allOrgTierData = new List<OrgTierLevelData>();

            OrgTierLevelData orgTierData = new OrgTierLevelData();

            bool repeat = true;

            using (SqlConnection connection = new SqlConnection(data.FromConnectionString))
            {
                connection.Open();

                while (repeat)
                {
                    //Get Org Tier
                    string query = "SELECT * FROM dbo.OrganizationalTier WHERE ID = @orgId";
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("orgId", data.FromFacility.OrganizationalTierID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    orgTierData.OrganizationalTier = await connection.QueryFirstOrDefaultAsync<OrganizationalTier>(query, parameters);

                    SwapID swapedOrgId = new SwapID();

                    swapedOrgId.OldID = orgTierData.OrganizationalTier.ID;
                    swapedOrgId.NewID = Guid.NewGuid();
                    orgTierData.SwapID = swapedOrgId;

                    orgTierIds.Add(orgTierData.SwapID);

                    orgTierData.OrganizationalTier.ID = swapedOrgId.NewID;

                    //Get Type
                    query = "SELECT * FROM dbo.OrganizationalTierType WHERE ID = @orgType";
                    parameters = new DynamicParameters();

                    parameters.Add("orgType", orgTierData.OrganizationalTier.OrganizationalTierTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    orgTierData.OrganizationalTierType = await connection.QueryFirstOrDefaultAsync<OrganizationalTierType>(query, parameters);

                    if(orgTierData.OrganizationalTier.ParentOrganizationalTierID == null || orgTierData.OrganizationalTier.ParentOrganizationalTierID == Guid.Empty)
                    {
                        repeat = false;
                    } 
                    else
                    {
                        data.FromFacility.OrganizationalTierID = orgTierData.OrganizationalTier.ParentOrganizationalTierID.Value;
                    }

                    //Get Type Attribute  Layout
                    query = "SELECT * FROM dbo.OrganizationalTierTypeAttributeLayout WHERE OrganizationalTierTypeID = @orgTypeId";
                    parameters = new DynamicParameters();

                    parameters.Add("orgTypeId", orgTierData.OrganizationalTierType.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    orgTierData.OrganizationalTierTypeAttributeLayout = await connection.QueryFirstOrDefaultAsync<OrganizationalTierTypeAttributeLayout>(query, parameters);

                    //If Layout isn't null, Get contents
                    if (orgTierData.OrganizationalTierTypeAttributeLayout != null)
                    {
                        //get Type Attribute Layout Detail
                        query = "SELECT * FROM dbo.OrganizationalTierTypeAttributeLayoutDetail WHERE OrganizationalTierTypeAttributeLayoutID = @layoutId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutId", orgTierData.OrganizationalTierTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        orgTierData.OrganizationalTierTypeAttributeLayoutDetail = await connection.QueryFirstOrDefaultAsync<OrganizationalTierTypeAttributeLayoutDetail>(query, parameters);

                        //Get Type Attribute Layout Contents
                        query = "SELECT * FROM dbo.OrganizationalTierTypeAttributeLayoutContent WHERE OrganizationalTierTypeAttributeLayoutID = @layoutId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutId", orgTierData.OrganizationalTierTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        orgTierData.OrganizationalTierTypeAttributeLayoutContent = await connection.QueryAsync<OrganizationalTierTypeAttributeLayoutContent>(query, parameters);

                        //Get Content Details, definitions, and Picklist data.
                        List<OrganizationalTierTypeAttributeLayoutContentDetail> detailList = new List<OrganizationalTierTypeAttributeLayoutContentDetail>();
                        foreach (OrganizationalTierTypeAttributeLayoutContent content in orgTierData.OrganizationalTierTypeAttributeLayoutContent)
                        {
                            query = "SELECT * FROM dbo.OrganizationalTierTypeAttributeLayoutContentDetail WHERE OrganizationalTierTypeAttributeLayoutContentID = @layoutContentId";
                            parameters = new DynamicParameters();

                            parameters.Add("layoutContentId", content.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            OrganizationalTierTypeAttributeLayoutContentDetail contentDetail = await connection.QueryFirstOrDefaultAsync<OrganizationalTierTypeAttributeLayoutContentDetail>(query, parameters);
                            if (contentDetail != null)
                            {
                                detailList.Add(contentDetail);
                            }

                            query = "SELECT * FROM dbo.OrganizationalTierTypeAttributeDefinition WHERE ID = @definitionId";
                            parameters = new DynamicParameters();

                            parameters.Add("definitionId", content.OrganizationalTierTypeAttributeDefinitionID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            orgTierData.OrganizationalTierTypeAttributeDefinition = await connection.QueryFirstOrDefaultAsync<OrganizationalTierTypeAttributeDefinition>(query, parameters);

                            if (orgTierData.OrganizationalTierTypeAttributeDefinition != null)
                            {
                                query = "SELECT * FROM dbo.PickList WHERE ID = @picklistId";
                                parameters = new DynamicParameters();

                                parameters.Add("picklistId", orgTierData.OrganizationalTierTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                orgTierData.PickList = await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters);

                                if (orgTierData.PickList != null)
                                {
                                    query = "SELECT * FROM dbo.PickListItem WHERE PickListID = @picklistId";
                                    parameters = new DynamicParameters();

                                    parameters.Add("picklistId", orgTierData.OrganizationalTierTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                    orgTierData.PickListItem = await connection.QueryAsync<PickListItem>(query, parameters);
                                }
                            }
                        }

                        orgTierData.OrganizationalTierTypeAttributeLayoutContentDetail = detailList;
                    }

                    allOrgTierData.Add(orgTierData);
                }

                connection.Close();

                orgTierData.OrganizationalTier.ModifiedByUsername = data.Username;
                orgTierData.OrganizationalTierType.ModifiedByUsername = data.Username;
                data.FromFacility.Name = data.NewFacilityName;
            }

            return allOrgTierData;
        }

        #endregion

        #region Facility Level Get Method

        public async Task<FacilityLevelData> GetAllFacilityLevelData(FacilityMoveData data)
        {
            FacilityLevelData facilityData = new FacilityLevelData();

            using (SqlConnection connection = new SqlConnection(data.FromConnectionString))
            {
                connection.Open();

                //Get Type
                string query = "SELECT * FROM dbo.FacilityType WHERE ID = @facilityTypeId";
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("facilityTypeId", data.FromFacility.FacilityTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                facilityData.FacilityType = await connection.QueryFirstAsync<FacilityType>(query, parameters);

                //Get Type Attribute  Layout
                query = "SELECT * FROM dbo.FacilityTypeAttributeLayout WHERE FacilityTypeID = @facilityTypeId";
                parameters = new DynamicParameters();

                parameters.Add("facilityTypeId", data.FromFacility.FacilityTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                facilityData.FacilityTypeAttributeLayout = await connection.QueryFirstOrDefaultAsync<FacilityTypeAttributeLayout>(query, parameters);

                //If Layout isn't null, Get contents
                if (facilityData.FacilityTypeAttributeLayout != null)
                {
                    //get Type Attribute Layout Detail
                    query = "SELECT * FROM dbo.FacilityTypeAttributeLayoutDetail WHERE FacilityTypeAttributeLayoutID = @layoutId";
                    parameters = new DynamicParameters();

                    parameters.Add("layoutId", facilityData.FacilityTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    facilityData.FacilityTypeAttributeLayoutDetail = await connection.QueryFirstOrDefaultAsync<FacilityTypeAttributeLayoutDetail>(query, parameters);

                    //Get Type Attribute Layout Contents
                    query = "SELECT * FROM dbo.FacilityTypeAttributeLayoutContent WHERE FacilityTypeAttributeLayoutID = @layoutId";
                    parameters = new DynamicParameters();

                    parameters.Add("layoutId", facilityData.FacilityTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    facilityData.FacilityTypeAttributeLayoutContent = await connection.QueryAsync<FacilityTypeAttributeLayoutContent>(query, parameters);

                    //Get Content Details, definitions, and Picklist data.
                    List<FacilityTypeAttributeLayoutContentDetail> detailList = new List<FacilityTypeAttributeLayoutContentDetail>();
                    foreach (FacilityTypeAttributeLayoutContent content in facilityData.FacilityTypeAttributeLayoutContent)
                    {
                        query = "SELECT * FROM dbo.FacilityTypeAttributeLayoutContentDetail WHERE FacilityTypeAttributeLayoutContentID = @layoutContentId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutContentId", content.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        FacilityTypeAttributeLayoutContentDetail contentDetail = await connection.QueryFirstOrDefaultAsync<FacilityTypeAttributeLayoutContentDetail>(query, parameters);
                        if (contentDetail != null)
                        {
                            detailList.Add(contentDetail);
                        }

                        query = "SELECT * FROM dbo.FacilityTypeAttributeDefinition WHERE ID = @definitionId";
                        parameters = new DynamicParameters();

                        parameters.Add("definitionId", content.FacilityTypeAttributeDefinitionID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        facilityData.FacilityTypeAttributeDefinition = await connection.QueryFirstOrDefaultAsync<FacilityTypeAttributeDefinition>(query, parameters);

                        if(facilityData.FacilityTypeAttributeDefinition != null)
                        {
                            query = "SELECT * FROM dbo.PickList WHERE ID = @picklistId";
                            parameters = new DynamicParameters();

                            parameters.Add("picklistId", facilityData.FacilityTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            facilityData.PickList = await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters);

                            if(facilityData.PickList != null)
                            {
                                query = "SELECT * FROM dbo.PickListItem WHERE PickListID = @picklistId";
                                parameters = new DynamicParameters();

                                parameters.Add("picklistId", facilityData.FacilityTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                facilityData.PickListItem = await connection.QueryAsync<PickListItem>(query, parameters);
                            }
                        }
                    }

                    facilityData.FacilityTypeAttributeLayoutContentDetail = detailList;
                }

                connection.Close();

                data.FromFacility.ModifiedByUsername = data.Username;
                data.FromFacility.Created = DateTime.Now;
                data.FromFacility.Modified = DateTime.Now;
                facilityData.FacilityType.ModifiedByUsername = data.Username;
                data.FromFacility.Name = data.NewFacilityName;

                facilityData.Facility = data.FromFacility;
            }

            return facilityData;
        }
        #endregion

        #region Tier Level Get Method

        public async Task<IEnumerable<FacilityTierLevelData>> GetAllFacilityTierLevelData(IEnumerable<FacilityTier> tiers, string connectionStr)
        {
            List<FacilityTierLevelData> facilityTierLevelData = new List<FacilityTierLevelData>();

            foreach(FacilityTier tier in tiers)
            {
                FacilityTierLevelData tierData = new FacilityTierLevelData();

                SwapID swapedTierId = new SwapID();

                swapedTierId.OldID = tier.ID;
                swapedTierId.NewID = Guid.NewGuid();
                tier.ID = swapedTierId.NewID;
                swapTierIds.Add(swapedTierId);
                tierData.SwapID = swapedTierId;

                tierData.FacilityTier = tier;

                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();

                    //Get tier Type
                    string query = "SELECT * FROM dbo.FacilityTierType WHERE ID = @tierTypeId";
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("tierTypeId", tier.FacilityTierTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    tierData.FacilityTierType = await connection.QuerySingleAsync<FacilityTierType>(query, parameters);

                    //Get tier Type Layout
                    query = "SELECT * FROM dbo.FacilityTierTypeAttributeLayout WHERE FacilityTierTypeID = @tierTypeId";
                    parameters = new DynamicParameters();

                    parameters.Add("tierTypeId", tier.FacilityTierTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    tierData.FacilityTierTypeAttributeLayout = await connection.QueryFirstOrDefaultAsync<FacilityTierTypeAttributeLayout>(query, parameters);

                    if(tierData.FacilityTierTypeAttributeLayout != null)
                    {
                        //Get tier layout detail
                        query = "SELECT * FROM dbo.FacilityTierTypeAttributeLayoutDetail WHERE FacilityTierTypeAttributeLayoutID = @layoutId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutId", tierData.FacilityTierTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        tierData.FacilityTierTypeAttributeLayoutDetail = await connection.QueryFirstOrDefaultAsync<FacilityTierTypeAttributeLayoutDetail>(query, parameters);

                        //Get tier layout contents
                        query = "SELECT * FROM dbo.FacilityTierTypeAttributeLayoutContent WHERE FacilityTierTypeAttributeLayoutID = @layoutId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutId", tierData.FacilityTierTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        tierData.FacilityTierTypeAttributeLayoutContent = await connection.QueryAsync<FacilityTierTypeAttributeLayoutContent>(query, parameters);

                        List<FacilityTierTypeAttributeLayoutContentDetail> detailList = new List<FacilityTierTypeAttributeLayoutContentDetail>();
                        foreach (FacilityTierTypeAttributeLayoutContent content in tierData.FacilityTierTypeAttributeLayoutContent)
                        {
                            query = "SELECT * FROM dbo.FacilityTierTypeAttributeLayoutContentDetail WHERE FacilityTierTypeAttributeLayoutContentID = @layoutContentId";
                            parameters = new DynamicParameters();

                            parameters.Add("layoutContentId", content.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            FacilityTierTypeAttributeLayoutContentDetail contentDetail = await connection.QueryFirstOrDefaultAsync<FacilityTierTypeAttributeLayoutContentDetail>(query, parameters);
                            if (contentDetail != null)
                            {
                                detailList.Add(contentDetail);
                            }

                            query = "SELECT * FROM dbo.FacilityTierTypeAttributeDefinition WHERE ID = @definitionId";
                            parameters = new DynamicParameters();

                            parameters.Add("definitionId", content.FacilityTierTypeAttributeDefinitionID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            tierData.FacilityTierTypeAttributeDefinition = await connection.QueryFirstOrDefaultAsync<FacilityTierTypeAttributeDefinition>(query, parameters);

                            if(tierData.FacilityTierTypeAttributeDefinition != null)
                            {
                                query = "SELECT * FROM dbo.PickList WHERE ID = @picklistId";
                                parameters = new DynamicParameters();

                                parameters.Add("picklistId", tierData.FacilityTierTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                tierData.PickList = await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters);

                                if (tierData.PickList != null)
                                {
                                    query = "SELECT * FROM dbo.PickListItem WHERE PickListID = @picklistId";
                                    parameters = new DynamicParameters();

                                    parameters.Add("picklistId", tierData.FacilityTierTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                    tierData.PickListItem = await connection.QueryAsync<PickListItem>(query, parameters);
                                }
                            }

                        }

                        tierData.FacilityTierTypeAttributeLayoutContentDetail = detailList;

                    }
                    connection.Close();

                }

                facilityTierLevelData.Add(tierData);
            }

            return facilityTierLevelData;
        }

        #endregion

        #region Item Level Get Methods

        public async Task<IEnumerable<ItemLevelData>> GetItemLevelData(IEnumerable<Item> items, string connectionStr)
        {
            List<ItemLevelData> itemLevelData = new List<ItemLevelData>();

            foreach(Item item in items)
            {
                ItemLevelData itemData = new ItemLevelData();

                SwapID swapedItemId = new SwapID();

                swapedItemId.OldID = item.ID;
                swapedItemId.NewID = Guid.NewGuid();
                item.ID = swapedItemId.NewID;
                swapItemIds.Add(swapedItemId);

                itemData.Item = item;

                using(SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();

                    //Get tier Type
                    string query = "SELECT * FROM dbo.ItemType WHERE ID = @itemTypeId";
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("itemTypeId", item.ItemTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    itemData.ItemType = await connection.QuerySingleAsync<ItemType>(query, parameters);

                    //Get tier Type Layout
                    query = "SELECT * FROM dbo.ItemTypeAttributeLayout WHERE ItemTypeID = @itemTypeId";
                    parameters = new DynamicParameters();

                    parameters.Add("itemTypeId", item.ItemTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    itemData.ItemTypeAttributeLayout = await connection.QueryFirstOrDefaultAsync<ItemTypeAttributeLayout>(query, parameters);

                    if (itemData.ItemTypeAttributeLayout != null)
                    {
                        //Get tier layout detail
                        query = "SELECT * FROM dbo.ItemTypeAttributeLayoutDetail WHERE ItemTypeAttributeLayoutID = @layoutId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutId", itemData.ItemTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        itemData.ItemTypeAttributeLayoutDetail = await connection.QueryFirstOrDefaultAsync<ItemTypeAttributeLayoutDetail>(query, parameters);

                        //Get tier layout contents
                        query = "SELECT * FROM dbo.ItemTypeAttributeLayoutContent WHERE ItemTypeAttributeLayoutID = @layoutId";
                        parameters = new DynamicParameters();

                        parameters.Add("layoutId", itemData.ItemTypeAttributeLayout.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        itemData.ItemTypeAttributeLayoutContent = await connection.QueryAsync<ItemTypeAttributeLayoutContent>(query, parameters);

                        List<ItemTypeAttributeLayoutContentDetail> detailList = new List<ItemTypeAttributeLayoutContentDetail>();
                        foreach (ItemTypeAttributeLayoutContent content in itemData.ItemTypeAttributeLayoutContent)
                        {
                            query = "SELECT * FROM dbo.ItemTypeAttributeLayoutContentDetail WHERE ItemTypeAttributeLayoutContentID = @layoutContentId";
                            parameters = new DynamicParameters();

                            parameters.Add("layoutContentId", content.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            ItemTypeAttributeLayoutContentDetail contentDetail = await connection.QueryFirstOrDefaultAsync<ItemTypeAttributeLayoutContentDetail>(query, parameters);
                            if (contentDetail != null)
                            {
                                detailList.Add(contentDetail);
                            }

                            query = "SELECT * FROM dbo.ItemTypeAttributeDefinition WHERE ID = @definitionId";
                            parameters = new DynamicParameters();

                            parameters.Add("definitionId", content.ItemTypeAttributeDefinitionID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            itemData.ItemTypeAttributeDefinition = await connection.QueryFirstOrDefaultAsync<ItemTypeAttributeDefinition>(query, parameters);

                            if (itemData.ItemTypeAttributeDefinition != null)
                            {
                                query = "SELECT * FROM dbo.PickList WHERE ID = @picklistId";
                                parameters = new DynamicParameters();

                                parameters.Add("picklistId", itemData.ItemTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                itemData.PickList = await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters);

                                if (itemData.PickList != null)
                                {
                                    query = "SELECT * FROM dbo.PickListItem WHERE PickListID = @picklistId";
                                    parameters = new DynamicParameters();

                                    parameters.Add("picklistId", itemData.ItemTypeAttributeDefinition.PickListID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                                    itemData.PickListItem = await connection.QueryAsync<PickListItem>(query, parameters);
                                }
                            }

                        }

                        itemData.ItemTypeAttributeLayoutContentDetail = detailList;

                    }

                    connection.Close();

                }

                itemLevelData.Add(itemData);

            }

            return itemLevelData;
        }

        #endregion

        #region Query Insert Methods

        //------------------------------------------------Move Facility

        public async Task<MoverStatusMessage> ExecuteOrgTierMoveQueryAsync(IEnumerable<OrgTierLevelData> orgData, string connectionStr)
        {
            MoverStatusMessage message = new MoverStatusMessage();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();

                    foreach(OrgTierLevelData orgTierLevelData in orgData)
                    {
                        //Check if Facility Type exists
                        string query = "SELECT * FROM dbo.OrganizationalTierType WHERE ID = @orgTypeId";
                        DynamicParameters parameters = new DynamicParameters();

                        parameters.Add("@orgTypeId", orgTierLevelData.OrganizationalTier.OrganizationalTierTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        bool doesTypeExist = (await connection.QueryFirstOrDefaultAsync<OrganizationalTierType>(query, parameters) != null);

                        //Check if picklist exist
                        bool doesPickListExist = false;
                        if (orgTierLevelData.PickList != null)
                        {
                            query = "SELECT * FROM dbo.PickList WHERE ID = @Id";
                            parameters = new DynamicParameters();

                            parameters.Add("Id", orgTierLevelData.PickList.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            doesPickListExist = (await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters) != null);
                        }

                        if (!doesTypeExist)
                        {
                            //Insert facility type first
                            query = "INSERT INTO [dbo].[OrganizationalTierType]([ID], [Description], [Name], [InternalType], [Protect], [Created], [Modified], [ModifiedByUsername]) " +
                                            "VALUES (@ID, @Description, @Name, @InternalType, @Protect, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                orgTierLevelData.OrganizationalTierType.ID,
                                orgTierLevelData.OrganizationalTierType.Description,
                                orgTierLevelData.OrganizationalTierType.Name,
                                orgTierLevelData.OrganizationalTierType.InternalType,
                                orgTierLevelData.OrganizationalTierType.Protect,
                                orgTierLevelData.OrganizationalTierType.Created,
                                orgTierLevelData.OrganizationalTierType.Modified,
                                orgTierLevelData.OrganizationalTierType.ModifiedByUsername
                            });

                            //Insert facility layout
                            if(orgTierLevelData.OrganizationalTierTypeAttributeLayout != null)
                            {
                                query = "INSERT INTO [dbo].[OrganizationalTierTypeAttributeLayout]([ID], [OrganizationalTierTypeID], [NumberOfColumns], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @OrganizationalTierTypeID, @NumberOfColumns, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayout.ID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayout.OrganizationalTierTypeID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayout.NumberOfColumns,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayout.Created,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayout.Modified,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayout.ModifiedByUsername
                                });

                                //Insert facility layout details
                                query = "INSERT INTO [dbo].[OrganizationalTierTypeAttributeLayoutDetail]([ID], [OrganizationalTierTypeAttributeLayoutID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @OrganizationalTierTypeAttributeLayoutID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.ID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.OrganizationalTierTypeAttributeLayoutID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.LanguageID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.Name,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.Created,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.Modified,
                                    orgTierLevelData.OrganizationalTierTypeAttributeLayoutDetail.ModifiedByUsername
                                });
                            }

                            //Insert PickList
                            if (orgTierLevelData.PickList != null && !doesPickListExist)
                            {
                                query = "INSERT INTO [dbo].[PickList]([ID], [Name], [Description], [Protected], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                "VALUES (@ID, @Name, @Description, @Protected, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                await connection.ExecuteAsync(query, new
                                {
                                    orgTierLevelData.PickList.ID,
                                    orgTierLevelData.PickList.Name,
                                    orgTierLevelData.PickList.Description,
                                    orgTierLevelData.PickList.Protected,
                                    orgTierLevelData.PickList.Active,
                                    orgTierLevelData.PickList.Created,
                                    orgTierLevelData.PickList.Modified,
                                    orgTierLevelData.PickList.ModifiedByUsername,
                                    orgTierLevelData.PickList.ExternalReference,
                                });

                                //Insert Picklist items
                                foreach (PickListItem item in orgTierLevelData.PickListItem)
                                {
                                    query = "INSERT INTO [dbo].[PickListItem]([ID], [PickListID], [Value], [SortOrder], [FormDocumentID], [HexColor], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                    "VALUES (@ID, @PickListID, @Value, @SortOrder, @FormDocumentID, @HexColor, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        item.ID,
                                        item.PickListID,
                                        item.Value,
                                        item.SortOrder,
                                        item.FormDocumentID,
                                        item.HexColor,
                                        item.Active,
                                        item.Created,
                                        item.Modified,
                                        item.ModifiedByUsername,
                                        item.ExternalReference,
                                    });
                                }
                            }

                            //Insert facility attribute definition
                            if (orgTierLevelData.OrganizationalTierTypeAttributeDefinition != null)
                            {
                                query = "INSERT INTO [dbo].[OrganizationalTierTypeAttributeDefinition]([ID], [DataType], [PickListID], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @DataType, @PickListID, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinition.ID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinition.DataType,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinition.PickListID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinition.Created,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinition.Modified,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinition.ModifiedByUsername
                                });

                                //Insert facility attribute definition detail
                                query = "INSERT INTO [dbo].[OrganizationalTierTypeAttributeDefinitionDetail]([ID], [OrganizationalTierTypeAttributeDefinitionID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @OrganizationalTierTypeAttributeDefinitionID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.ID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.OrganizationalTierTypeAttributeDefinitionID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.LanguageID,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.Name,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.Created,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.Modified,
                                    orgTierLevelData.OrganizationalTierTypeAttributeDefinitionDetail.ModifiedByUsername
                                });
                            }

                            //Insert facility layout content
                            if(orgTierLevelData.OrganizationalTierTypeAttributeLayoutContent != null)
                            {
                                foreach (OrganizationalTierTypeAttributeLayoutContent orgTierContent in orgTierLevelData.OrganizationalTierTypeAttributeLayoutContent)
                                {
                                    query = "INSERT INTO [dbo].[OrganizationalTierTypeAttributeLayoutContent]([ID], [OrganizationalTierTypeAttributeLayoutID], [OrganizationalTierTypeAttributeDefinitionID], [SortOrder], [ReadOnly], [Required], [Created], [Modified], [ModifiedByUsername]) " +
                                                        "VALUES (@ID, @OrganizationalTierTypeAttributeLayoutID, @OrganizationalTierTypeAttributeDefinitionID, @SortOrder, @ReadOnly, @Required, @Created, @Modified, @ModifiedByUsername)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        orgTierContent.ID,
                                        orgTierContent.OrganizationalTierTypeAttributeLayoutID,
                                        orgTierContent.OrganizationalTierTypeAttributeDefinitionID,
                                        orgTierContent.SortOrder,
                                        orgTierContent.ReadOnly,
                                        orgTierContent.Required,
                                        orgTierContent.Created,
                                        orgTierContent.Modified,
                                        orgTierContent.ModifiedByUsername
                                    });

                                    //Insert layout content details
                                    foreach (OrganizationalTierTypeAttributeLayoutContentDetail detail in orgTierLevelData.OrganizationalTierTypeAttributeLayoutContentDetail)
                                    {
                                        query = "INSERT INTO [dbo].[OrganizationalTierTypeAttributeLayoutContentDetail]([ID], [OrganizationalTierTypeAttributeLayoutContentID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                            "VALUES (@ID, @OrganizationalTierTypeAttributeLayoutContentID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                        await connection.ExecuteAsync(query, new
                                        {
                                            detail.ID,
                                            detail.OrganizationalTierTypeAttributeLayoutContentID,
                                            detail.LanguageID,
                                            detail.Created,
                                            detail.Modified,
                                            detail.ModifiedByUsername
                                        });
                                    }
                                }
                            }
                        }

                        query = "INSERT INTO [dbo].[OrganizationalTier]([ID], [Description], [Name], [OrganizationalTierTypeID], [ParentOrganizationalTierID], [Active], [Deleted], [Created], [Modified], [ModifiedByUsername]) " +
                                        "VALUES (@ID, @Description, @Name, @OrganizationalTierTypeID, @ParentOrganizationalTierID, @Active, @Deleted, @Created, @Modified, @ModifiedByUsername)";
                        await connection.ExecuteAsync(query, new
                        {
                            orgTierLevelData.OrganizationalTier.ID,
                            orgTierLevelData.OrganizationalTier.Description,
                            orgTierLevelData.OrganizationalTier.Name,
                            orgTierLevelData.OrganizationalTier.OrganizationalTierTypeID,
                            orgTierLevelData.OrganizationalTier.ParentOrganizationalTierID,
                            orgTierLevelData.OrganizationalTier.Active,
                            orgTierLevelData.OrganizationalTier.Deleted,
                            orgTierLevelData.OrganizationalTier.Created,
                            orgTierLevelData.OrganizationalTier.Modified,
                            orgTierLevelData.OrganizationalTier.ModifiedByUsername
                        });
                    }
                }
            }
            catch (Exception e)
            {
                message.FailedMessage = "Error, Failed to insert facility level data in new environment error message: " + e.Message;
            }

            message.CompleteMessage = "Completed Org Tier Level";

            return message;
        }

        public async Task<MoverStatusMessage> ExecuteFacilityMoveQueryAsync(FacilityLevelData facilityLevelData, string connectionStr)
        {
            MoverStatusMessage message = new MoverStatusMessage();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();

                    //Check if Facility Type exists
                    string query = "SELECT * FROM dbo.FacilityType WHERE ID = @facilityTypeId";
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@facilityTypeId", facilityLevelData.Facility.FacilityTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                    bool doesTypeExist = (await connection.QueryFirstOrDefaultAsync<FacilityType>(query, parameters) != null);

                    //Check if picklist exist
                    bool doesPickListExist = false;
                    if (facilityLevelData.PickList != null)
                    {
                        query = "SELECT * FROM dbo.PickList WHERE ID = @Id";
                        parameters = new DynamicParameters();

                        parameters.Add("Id", facilityLevelData.PickList.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        doesPickListExist = (await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters) != null);
                    }

                    if (!doesTypeExist)
                    {
                        //Insert facility type first
                        query = "INSERT INTO [dbo].[FacilityType]([ID], [Description], [Name], [Active], [Created], [Modified], [ModifiedByUsername]) " +
                                        "VALUES (@ID, @Description, @Name, @Active, @Created, @Modified, @ModifiedByUsername)";
                        await connection.ExecuteAsync(query, new
                        {
                            facilityLevelData.FacilityType.ID,
                            facilityLevelData.FacilityType.Description,
                            facilityLevelData.FacilityType.Name,
                            facilityLevelData.FacilityType.Active,
                            facilityLevelData.FacilityType.Created,
                            facilityLevelData.FacilityType.Modified,
                            facilityLevelData.FacilityType.ModifiedByUsername
                        });

                        //Insert facility layout
                        if (facilityLevelData.FacilityTypeAttributeLayout != null)
                        {
                            query = "INSERT INTO [dbo].[FacilityTypeAttributeLayout]([ID], [FacilityTypeID], [NumberOfColumns], [SortOrder], [Created], [Modified], [ModifiedByUsername]) " +
                                            "VALUES (@ID, @FacilityTypeID, @NumberOfColumns, @SortOrder, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                facilityLevelData.FacilityTypeAttributeLayout.ID,
                                facilityLevelData.FacilityTypeAttributeLayout.FacilityTypeID,
                                facilityLevelData.FacilityTypeAttributeLayout.NumberOfColumns,
                                facilityLevelData.FacilityTypeAttributeLayout.SortOrder,
                                facilityLevelData.FacilityTypeAttributeLayout.Created,
                                facilityLevelData.FacilityTypeAttributeLayout.Modified,
                                facilityLevelData.FacilityTypeAttributeLayout.ModifiedByUsername
                            });

                            //Insert facility layout details
                            query = "INSERT INTO [dbo].[FacilityTypeAttributeLayoutDetail]([ID], [FacilityTypeAttributeLayoutID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @FacilityTypeAttributeLayoutID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.ID,
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.FacilityTypeAttributeLayoutID,
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.LanguageID,
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.Name,
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.Created,
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.Modified,
                                facilityLevelData.FacilityTypeAttributeLayoutDetail.ModifiedByUsername
                            });
                        }

                        //Insert PickList
                        if (facilityLevelData.PickList != null && !doesPickListExist)
                        {
                            query = "INSERT INTO [dbo].[PickList]([ID], [Name], [Description], [Protected], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                            "VALUES (@ID, @Name, @Description, @Protected, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                            await connection.ExecuteAsync(query, new
                            {
                                facilityLevelData.PickList.ID,
                                facilityLevelData.PickList.Name,
                                facilityLevelData.PickList.Description,
                                facilityLevelData.PickList.Protected,
                                facilityLevelData.PickList.Active,
                                facilityLevelData.PickList.Created,
                                facilityLevelData.PickList.Modified,
                                facilityLevelData.PickList.ModifiedByUsername,
                                facilityLevelData.PickList.ExternalReference,
                            });

                            //Insert Picklist items
                            foreach (PickListItem item in facilityLevelData.PickListItem)
                            {
                                query = "INSERT INTO [dbo].[PickListItem]([ID], [PickListID], [Value], [SortOrder], [FormDocumentID], [HexColor], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                "VALUES (@ID, @PickListID, @Value, @SortOrder, @FormDocumentID, @HexColor, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                await connection.ExecuteAsync(query, new
                                {
                                    item.ID,
                                    item.PickListID,
                                    item.Value,
                                    item.SortOrder,
                                    item.FormDocumentID,
                                    item.HexColor,
                                    item.Active,
                                    item.Created,
                                    item.Modified,
                                    item.ModifiedByUsername,
                                    item.ExternalReference,
                                });
                            }
                        }

                        //Insert facility attribute definition
                        if (facilityLevelData.FacilityTypeAttributeDefinition != null)
                        {
                            query = "INSERT INTO [dbo].[FacilityTypeAttributeDefinition]([ID], [DataType], [PickListID], [Created], [Modified], [ModifiedByUsername]) " +
                                            "VALUES (@ID, @DataType, @PickListID, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                facilityLevelData.FacilityTypeAttributeDefinition.ID,
                                facilityLevelData.FacilityTypeAttributeDefinition.DataType,
                                facilityLevelData.FacilityTypeAttributeDefinition.PickListID,
                                facilityLevelData.FacilityTypeAttributeDefinition.Created,
                                facilityLevelData.FacilityTypeAttributeDefinition.Modified,
                                facilityLevelData.FacilityTypeAttributeDefinition.ModifiedByUsername
                            });

                            //Insert facility attribute definition detail
                            query = "INSERT INTO [dbo].[FacilityTypeAttributeDefinitionDetail]([ID], [FacilityTypeAttributeDefinitionID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @FacilityTypeAttributeDefinitionID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.ID,
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.FacilityTypeAttributeDefinitionID,
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.LanguageID,
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.Name,
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.Created,
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.Modified,
                                facilityLevelData.FacilityTypeAttributeDefinitionDetail.ModifiedByUsername
                            });
                        }

                        //Insert facility layout content
                        if(facilityLevelData.FacilityTypeAttributeLayoutContent != null)
                        {
                            foreach (FacilityTypeAttributeLayoutContent facilityContent in facilityLevelData.FacilityTypeAttributeLayoutContent)
                            {
                                query = "INSERT INTO [dbo].[FacilityTypeAttributeLayoutContent]([ID], [FacilityTypeAttributeLayoutID], [FacilityTypeAttributeDefinitionID], [SortOrder], [ReadOnly], [Required], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @FacilityTypeAttributeLayoutID, @FacilityTypeAttributeDefinitionID, @SortOrder, @ReadOnly, @Required, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    facilityContent.ID,
                                    facilityContent.FacilityTypeAttributeLayoutID,
                                    facilityContent.FacilityTypeAttributeDefinitionID,
                                    facilityContent.SortOrder,
                                    facilityContent.ReadOnly,
                                    facilityContent.Required,
                                    facilityContent.Created,
                                    facilityContent.Modified,
                                    facilityContent.ModifiedByUsername
                                });

                                //Insert layout content details
                                foreach (FacilityTypeAttributeLayoutContentDetail detail in facilityLevelData.FacilityTypeAttributeLayoutContentDetail)
                                {
                                    query = "INSERT INTO [dbo].[FacilityTypeAttributeLayoutContentDetail]([ID], [FacilityTypeAttributeLayoutContentID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                        "VALUES (@ID, @FacilityTypeAttributeLayoutContentID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        detail.ID,
                                        detail.FacilityTypeAttributeLayoutContentID,
                                        detail.LanguageID,
                                        detail.Created,
                                        detail.Modified,
                                        detail.ModifiedByUsername
                                    });
                                }
                            }
                        }
                    }

                    query = "INSERT INTO [dbo].[Facility]([ID], [Description], [Name], [SystemOfMeasure], [ModelURI], [FacilityTypeID], [OrganizationalTierID], [Code1PickListId], [Code2PickListId], [Active], [Deleted], [Created], [Modified], [ModifiedByUsername]) " +
                                    "VALUES (@ID, @Description, @Name, @SystemOfMeasure, @ModelURI, @FacilityTypeID, @OrganizationalTierID, @Code1PickListId, @Code2PickListId, @Active, @Deleted, @Created, @Modified, @ModifiedByUsername)";
                    await connection.ExecuteAsync(query, new
                    {
                        facilityLevelData.Facility.ID,
                        facilityLevelData.Facility.Description,
                        facilityLevelData.Facility.Name,
                        facilityLevelData.Facility.SystemOfMeasure,
                        facilityLevelData.Facility.ModelURI,
                        facilityLevelData.Facility.FacilityTypeID,
                        facilityLevelData.Facility.OrganizationalTierID,
                        facilityLevelData.Facility.Code1PickListId,
                        facilityLevelData.Facility.Code2PickListId,
                        facilityLevelData.Facility.Active,
                        facilityLevelData.Facility.Deleted,
                        facilityLevelData.Facility.Created,
                        facilityLevelData.Facility.Modified,
                        facilityLevelData.Facility.ModifiedByUsername
                    });
                }
            }
            catch (Exception e)
            {
                message.FailedMessage = "Error, Failed to insert facility level data in new environment error message: " + e.Message;
            }

            message.CompleteMessage = "Completed Facility Level";

            return message;
        }

        //------------------------------------------------Move Tiers
        public async Task<MoverStatusMessage> ExecuteFacilityTierMoveQueryAsync(IEnumerable<FacilityTierLevelData> facilityTierLevelData, string connectionStr)
        {
            MoverStatusMessage message = new MoverStatusMessage();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();

                    foreach (FacilityTierLevelData facilityTier in facilityTierLevelData)
                    {
                        //Check if Facility Type exists
                        string query = "SELECT * FROM dbo.FacilityType WHERE ID = @facilityTypeId";
                        DynamicParameters parameters = new DynamicParameters();

                        parameters.Add("@facilityTypeId", facilityTier.FacilityTier.FacilityTierTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        bool doesTypeExist = (await connection.QueryFirstOrDefaultAsync<FacilityType>(query, parameters) != null);

                        //Check if picklist exist
                        bool doesPickListExist = false;
                        if (facilityTier.PickList != null)
                        {
                            query = "SELECT * FROM dbo.PickList WHERE ID = @Id";
                            parameters = new DynamicParameters();

                            parameters.Add("Id", facilityTier.PickList.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            doesPickListExist = (await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters) != null);
                        }

                        if(!doesTypeExist)
                        {
                            //Insert facility type first
                            query = "INSERT INTO [dbo].[FacilityTierType]([ID], [Description], [Name], [Active], [Created], [Modified], [ModifiedByUsername]) " +
                                            "VALUES (@ID, @Description, @Name, @Active, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                facilityTier.FacilityTierType.ID,
                                facilityTier.FacilityTierType.Description,
                                facilityTier.FacilityTierType.Name,
                                facilityTier.FacilityTierType.Active,
                                facilityTier.FacilityTierType.Created,
                                facilityTier.FacilityTierType.Modified,
                                facilityTier.FacilityTierType.ModifiedByUsername
                            });

                            //Insert facility layout
                            if (facilityTier.FacilityTierTypeAttributeLayout != null)
                            {
                                query = "INSERT INTO [dbo].[FacilityTierTypeAttributeLayout]([ID], [FacilityTierTypeID], [NumberOfColumns], [SortOrder], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @FacilityTierTypeID, @NumberOfColumns, @SortOrder, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    facilityTier.FacilityTierTypeAttributeLayout.ID,
                                    facilityTier.FacilityTierTypeAttributeLayout.FacilityTierTypeID,
                                    facilityTier.FacilityTierTypeAttributeLayout.NumberOfColumns,
                                    facilityTier.FacilityTierTypeAttributeLayout.SortOrder,
                                    facilityTier.FacilityTierTypeAttributeLayout.Created,
                                    facilityTier.FacilityTierTypeAttributeLayout.Modified,
                                    facilityTier.FacilityTierTypeAttributeLayout.ModifiedByUsername
                                });

                                //Insert facility layout details
                                query = "INSERT INTO [dbo].[FacilityTierTypeAttributeLayoutDetail]([ID], [FacilityTierTypeAttributeLayoutID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @FacilityTierTypeAttributeLayoutID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.ID,
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.FacilityTierTypeAttributeLayoutID,
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.LanguageID,
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.Name,
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.Created,
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.Modified,
                                    facilityTier.FacilityTierTypeAttributeLayoutDetail.ModifiedByUsername
                                });
                            }

                            //Insert PickList
                            if (facilityTier.PickList != null && !doesPickListExist)
                            {
                                query = "INSERT INTO [dbo].[PickList]([ID], [Name], [Description], [Protected], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                "VALUES (@ID, @Name, @Description, @Protected, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                await connection.ExecuteAsync(query, new
                                {
                                    facilityTier.PickList.ID,
                                    facilityTier.PickList.Name,
                                    facilityTier.PickList.Description,
                                    facilityTier.PickList.Protected,
                                    facilityTier.PickList.Active,
                                    facilityTier.PickList.Created,
                                    facilityTier.PickList.Modified,
                                    facilityTier.PickList.ModifiedByUsername,
                                    facilityTier.PickList.ExternalReference,
                                });

                                //Insert Picklist items
                                foreach (PickListItem item in facilityTier.PickListItem)
                                {
                                    query = "INSERT INTO [dbo].[PickListItem]([ID], [PickListID], [Value], [SortOrder], [FormDocumentID], [HexColor], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                    "VALUES (@ID, @PickListID, @Value, @SortOrder, @FormDocumentID, @HexColor, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        item.ID,
                                        item.PickListID,
                                        item.Value,
                                        item.SortOrder,
                                        item.FormDocumentID,
                                        item.HexColor,
                                        item.Active,
                                        item.Created,
                                        item.Modified,
                                        item.ModifiedByUsername,
                                        item.ExternalReference,
                                    });
                                }
                            }

                            //Insert facility attribute definition
                            if (facilityTier.FacilityTierTypeAttributeDefinition != null)
                            {
                                query = "INSERT INTO [dbo].[FacilityTierTypeAttributeDefinition]([ID], [DataType], [PickListID], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @DataType, @PickListID, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    facilityTier.FacilityTierTypeAttributeDefinition.ID,
                                    facilityTier.FacilityTierTypeAttributeDefinition.DataType,
                                    facilityTier.FacilityTierTypeAttributeDefinition.PickListID,
                                    facilityTier.FacilityTierTypeAttributeDefinition.Created,
                                    facilityTier.FacilityTierTypeAttributeDefinition.Modified,
                                    facilityTier.FacilityTierTypeAttributeDefinition.ModifiedByUsername
                                });

                                //Insert facility attribute definition detail
                                query = "INSERT INTO [dbo].[FacilityTierTypeAttributeDefinitionDetail]([ID], [FacilityTierTypeAttributeDefinitionID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @FacilityTierTypeAttributeDefinitionID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.ID,
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.FacilityTierTypeAttributeDefinitionID,
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.LanguageID,
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.Name,
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.Created,
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.Modified,
                                    facilityTier.FacilityTierTypeAttributeDefinitionDetail.ModifiedByUsername
                                });
                            }

                            //Insert facility layout content
                            if (facilityTier.FacilityTierTypeAttributeLayoutContent != null)
                            {
                                foreach (FacilityTierTypeAttributeLayoutContent facilityTierContent in facilityTier.FacilityTierTypeAttributeLayoutContent)
                                {
                                    query = "INSERT INTO [dbo].[FacilityTierTypeAttributeLayoutContent]([ID], [FacilityTierTypeAttributeLayoutID], [FacilityTierTypeAttributeDefinitionID], [SortOrder], [ReadOnly], [Required], [Created], [Modified], [ModifiedByUsername]) " +
                                                        "VALUES (@ID, @FacilityTypeAttributeLayoutID, @FacilityTypeAttributeDefinitionID, @SortOrder, @ReadOnly, @Required, @Created, @Modified, @ModifiedByUsername)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        facilityTierContent.ID,
                                        facilityTierContent.FacilityTierTypeAttributeLayoutID,
                                        facilityTierContent.FacilityTierTypeAttributeDefinitionID,
                                        facilityTierContent.SortOrder,
                                        facilityTierContent.ReadOnly,
                                        facilityTierContent.Required,
                                        facilityTierContent.Created,
                                        facilityTierContent.Modified,
                                        facilityTierContent.ModifiedByUsername
                                    });

                                    //Insert layout content details
                                    foreach (FacilityTierTypeAttributeLayoutContentDetail detail in facilityTier.FacilityTierTypeAttributeLayoutContentDetail)
                                    {
                                        query = "INSERT INTO [dbo].[FacilityTierTypeAttributeLayoutContentDetail]([ID], [FacilityTierTypeAttributeLayoutContentID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                            "VALUES (@ID, @FacilityTypeAttributeLayoutContentID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                        await connection.ExecuteAsync(query, new
                                        {
                                            detail.ID,
                                            detail.FacilityTierTypeAttributeLayoutContentID,
                                            detail.LanguageID,
                                            detail.Created,
                                            detail.Modified,
                                            detail.ModifiedByUsername
                                        });
                                    }
                                }
                            }
                        }

                        query = "INSERT INTO [dbo].[FacilityTier]([ID], [Description], [Name], [FacilityID], [ParentFacilityTierID], [FacilityTierTypeID], [FacilityPath], [ExternalReference], [Active], [Deleted], [Created], [Modified], [ModifiedByUsername]) " +
                                        "VALUES (@ID, @Description, @Name, @FacilityID, @ParentFacilityTierID, @FacilityTierTypeID, @FacilityPath, @ExternalReference, @Active, @Deleted, @Created, @Modified, @ModifiedByUsername)";
                        await connection.ExecuteAsync(query, new
                        {
                            facilityTier.FacilityTier.ID,
                            facilityTier.FacilityTier.Description,
                            facilityTier.FacilityTier.Name,
                            facilityTier.FacilityTier.FacilityID,
                            facilityTier.FacilityTier.ParentFacilityTierID,
                            facilityTier.FacilityTier.FacilityTierTypeID,
                            facilityTier.FacilityTier.FacilityPath,
                            facilityTier.FacilityTier.ExternalReference,
                            facilityTier.FacilityTier.Active,
                            facilityTier.FacilityTier.Deleted,
                            facilityTier.FacilityTier.Created,
                            facilityTier.FacilityTier.Modified,
                            facilityTier.FacilityTier.ModifiedByUsername
                        });
                    }
                }
            }
            catch (Exception e)
            {
                message.FailedMessage = "Error, Failed to insert facility tier level data in new environment error message: " + e.Message;
            }

            message.CompleteMessage = "Completed Tier Level";

            return message;
        }

        //Move Items
        public async Task<MoverStatusMessage> ExecuteItemMoveQueryAsync(IEnumerable<ItemLevelData> itemLevelData, string connectionStr)
        {
            MoverStatusMessage message = new MoverStatusMessage();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();

                    foreach (ItemLevelData item in itemLevelData)
                    {
                        //Check if Facility Type exists
                        string query = "SELECT * FROM dbo.FacilityType WHERE ID = @itemTypeId";
                        DynamicParameters parameters = new DynamicParameters();

                        parameters.Add("@itemTypeId", item.Item.ItemTypeID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                        bool doesTypeExist = (await connection.QueryFirstOrDefaultAsync<FacilityType>(query, parameters) != null);

                        //Check if picklist exist
                        bool doesPickListExist = false;
                        if (item.PickList != null)
                        {
                            query = "SELECT * FROM dbo.PickList WHERE ID = @Id";
                            parameters = new DynamicParameters();

                            parameters.Add("Id", item.PickList.ID, System.Data.DbType.Guid, System.Data.ParameterDirection.Input);
                            doesPickListExist = (await connection.QueryFirstOrDefaultAsync<PickList>(query, parameters) != null);
                        }

                        if (!doesTypeExist)
                        {
                            //Insert facility type first
                            query = "INSERT INTO [dbo].[ItemType]([ID], [Description], [Name], [Active], [Created], [Modified], [ModifiedByUsername]) " +
                                            "VALUES (@ID, @Description, @Name, @Active, @Created, @Modified, @ModifiedByUsername)";
                            await connection.ExecuteAsync(query, new
                            {
                                item.ItemType.ID,
                                item.ItemType.Description,
                                item.ItemType.Name,
                                item.ItemType.Active,
                                item.ItemType.Created,
                                item.ItemType.Modified,
                                item.ItemType.ModifiedByUsername
                            });

                            //Insert facility layout
                            if(item.ItemTypeAttributeLayout != null)
                            {
                                query = "INSERT INTO [dbo].[ItemTypeAttributeLayout]([ID], [ItemTypeID], [NumberOfColumns], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @ItemTypeID, @NumberOfColumns, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    item.ItemTypeAttributeLayout.ID,
                                    item.ItemTypeAttributeLayout.ItemTypeID,
                                    item.ItemTypeAttributeLayout.NumberOfColumns,
                                    item.ItemTypeAttributeLayout.Created,
                                    item.ItemTypeAttributeLayout.Modified,
                                    item.ItemTypeAttributeLayout.ModifiedByUsername
                                });

                                //Insert facility layout details
                                query = "INSERT INTO [dbo].[ItemTypeAttributeLayoutDetail]([ID], [ItemTypeAttributeLayoutID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @ItemTypeAttributeLayoutID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    item.ItemTypeAttributeLayoutDetail.ID,
                                    item.ItemTypeAttributeLayoutDetail.ItemTypeAttributeLayoutID,
                                    item.ItemTypeAttributeLayoutDetail.LanguageID,
                                    item.ItemTypeAttributeLayoutDetail.Name,
                                    item.ItemTypeAttributeLayoutDetail.Created,
                                    item.ItemTypeAttributeLayoutDetail.Modified,
                                    item.ItemTypeAttributeLayoutDetail.ModifiedByUsername
                                });
                            }

                            //Insert PickList
                            if (item.PickList != null && !doesPickListExist)
                            {
                                query = "INSERT INTO [dbo].[PickList]([ID], [Name], [Description], [Protected], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                "VALUES (@ID, @Name, @Description, @Protected, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                await connection.ExecuteAsync(query, new
                                {
                                    item.PickList.ID,
                                    item.PickList.Name,
                                    item.PickList.Description,
                                    item.PickList.Protected,
                                    item.PickList.Active,
                                    item.PickList.Created,
                                    item.PickList.Modified,
                                    item.PickList.ModifiedByUsername,
                                    item.PickList.ExternalReference,
                                });

                                //Insert Picklist items
                                foreach (PickListItem pickListItem in item.PickListItem)
                                {
                                    query = "INSERT INTO [dbo].[PickListItem]([ID], [PickListID], [Value], [SortOrder], [FormDocumentID], [HexColor], [Active], [Created], [Modified], [ModifiedByUsername], [ExternalReference]) " +
                                                    "VALUES (@ID, @PickListID, @Value, @SortOrder, @FormDocumentID, @HexColor, @Active, @Created, @Modified, @ModifiedByUsername, @ExternalReference)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        pickListItem.ID,
                                        pickListItem.PickListID,
                                        pickListItem.Value,
                                        pickListItem.SortOrder,
                                        pickListItem.FormDocumentID,
                                        pickListItem.HexColor,
                                        pickListItem.Active,
                                        pickListItem.Created,
                                        pickListItem.Modified,
                                        pickListItem.ModifiedByUsername,
                                        pickListItem.ExternalReference,
                                    });
                                }
                            }

                            //Insert facility attribute definition
                            if (item.ItemTypeAttributeDefinition != null)
                            {
                                query = "INSERT INTO [dbo].[ItemTypeAttributeDefinition]([ID], [DataType], [PickListID], [Created], [Modified], [ModifiedByUsername]) " +
                                                "VALUES (@ID, @DataType, @PickListID, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    item.ItemTypeAttributeDefinition.ID,
                                    item.ItemTypeAttributeDefinition.DataType,
                                    item.ItemTypeAttributeDefinition.PickListID,
                                    item.ItemTypeAttributeDefinition.Created,
                                    item.ItemTypeAttributeDefinition.Modified,
                                    item.ItemTypeAttributeDefinition.ModifiedByUsername
                                });

                                //Insert facility attribute definition detail
                                query = "INSERT INTO [dbo].[ItemTypeAttributeDefinitionDetail]([ID], [ItemTypeAttributeDefinitionID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                    "VALUES (@ID, @ItemTypeAttributeDefinitionID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                await connection.ExecuteAsync(query, new
                                {
                                    item.ItemTypeAttributeDefinitionDetail.ID,
                                    item.ItemTypeAttributeDefinitionDetail.ItemTypeAttributeDefinitionID,
                                    item.ItemTypeAttributeDefinitionDetail.LanguageID,
                                    item.ItemTypeAttributeDefinitionDetail.Name,
                                    item.ItemTypeAttributeDefinitionDetail.Created,
                                    item.ItemTypeAttributeDefinitionDetail.Modified,
                                    item.ItemTypeAttributeDefinitionDetail.ModifiedByUsername
                                });
                            }

                            //Insert facility layout content
                            if(item.ItemTypeAttributeLayoutContent != null)
                            {
                                foreach (ItemTypeAttributeLayoutContent itemContent in item.ItemTypeAttributeLayoutContent)
                                {
                                    query = "INSERT INTO [dbo].[ItemTypeAttributeLayoutContent]([ID], [ItemTypeAttributeLayoutID], [ItemTypeAttributeDefinitionID], [SortOrder], [ReadOnly], [Required], [Created], [Modified], [ModifiedByUsername]) " +
                                                        "VALUES (@ID, @ItemTypeAttributeLayoutID, @ItemTypeAttributeDefinitionID, @SortOrder, @ReadOnly, @Required, @Created, @Modified, @ModifiedByUsername)";
                                    await connection.ExecuteAsync(query, new
                                    {
                                        itemContent.ID,
                                        itemContent.ItemTypeAttributeLayoutID,
                                        itemContent.ItemTypeAttributeDefinitionID,
                                        itemContent.SortOrder,
                                        itemContent.ReadOnly,
                                        itemContent.Required,
                                        itemContent.Created,
                                        itemContent.Modified,
                                        itemContent.ModifiedByUsername
                                    });

                                    //Insert layout content details
                                    foreach (ItemTypeAttributeLayoutContentDetail detail in item.ItemTypeAttributeLayoutContentDetail)
                                    {
                                        query = "INSERT INTO [dbo].[ItemTypeAttributeLayoutContentDetail]([ID], [ItemTypeAttributeLayoutContentID], [LanguageID], [Name], [Created], [Modified], [ModifiedByUsername]) " +
                                                            "VALUES (@ID, @ItemTypeAttributeLayoutContentID, @LanguageID, @Name, @Created, @Modified, @ModifiedByUsername)";
                                        await connection.ExecuteAsync(query, new
                                        {
                                            detail.ID,
                                            detail.ItemTypeAttributeLayoutContentID,
                                            detail.LanguageID,
                                            detail.Created,
                                            detail.Modified,
                                            detail.ModifiedByUsername
                                        });
                                    }
                                }
                            }
                        }

                        query = "INSERT INTO [dbo].[Item]([ID], [Description], [Name], [FacilityID], [ParentItemID], [ItemTypeID], [AssestIndetifier], [AssetNumber], [ExternalReference], [Active], [GPSLongitude], [GPSLatitude], [InteropClientID], [New], [Created], [Modified], [ModifiedByUsername]) " +
                                        "VALUES (@ID, @Description, @Name, @FacilityID, @ParentItemID, @ItemTypeID, @AssestIndetifier, @AssetNumber, @ExternalReference, @Active, @GPSLongitude, @GPSLatitude, @InteropClientID, @New, @Created, @Modified, @ModifiedByUsername)";
                        await connection.ExecuteAsync(query, new
                        {
                            item.Item.ID,
                            item.Item.Description,
                            item.Item.Name,
                            item.Item.FacilityID,
                            item.Item.ParentItemID,
                            item.Item.ItemTypeID,
                            item.Item.AssestIndetifier,
                            item.Item.AssetNumber,
                            item.Item.ExternalReference,
                            item.Item.Active,
                            item.Item.GPSLongitude,
                            item.Item.GPSLatitude,
                            item.Item.InteropClientID,
                            item.Item.New,
                            item.Item.Created,
                            item.Item.Modified,
                            item.Item.ModifiedByUsername
                        });
                    }
                }
            }
            catch (Exception e)
            {
                message.FailedMessage = "Error, Failed to insert facility tier level data in new environment error message: " + e.Message;
            }

            message.CompleteMessage = "Complete";

            return message;
        }

        #endregion
    }
}
